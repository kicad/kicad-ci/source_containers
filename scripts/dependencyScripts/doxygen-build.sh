#!/bin/bash

########################################################################
# Test if 'sudo' is available, work around otherwise
#
# Ubuntu docker images run as root and do not have the sudo command available
########################################################################
if ! command -v sudo >/dev/null 2>&1;
then
    # sudo is not available
    if [[ "$EUID" == "0" ]];
    then
        # We are running as root, so use a dummy function that makes 'sudo command' just call 'command'
        function sudo()
        {
            $@
        }
    else
        echo "Sudo is not available, please run this script as root." >&2
        exit 1
    fi
fi

##
# Get the dependencies we need for build
##

sudo apt-get -y -qq install flex bison wget

wget -c https://ftp.osuosl.org/pub/blfs/conglomeration/doxygen/doxygen-1.9.4.src.tar.gz -O - | tar -xz

cd doxygen-1.9.4
sed -i '/mutex/a#include <utility>' src/cache.h

mkdir -v build
cd build

cmake -G "Unix Makefiles"         \
      -DCMAKE_BUILD_TYPE=Release  \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -Wno-dev ..

make
sudo make install

##
# Cleanup
##
sudo apt-get -y autoremove flex bison

cd ../..
rm -rf doxygen-1.9.4
