#!/bin/bash

source /etc/os-release

########################################################################
# Parse the arguments
########################################################################
case $1 in
-h|--help)
    echo "KiCad dependency installer for Fedora"
    echo ""
    echo "See the help in install-dependencies.sh for more information."
    exit
esac

USE_WXPHOENIX=1
INSTALL_XVFB=0


while (( "$#" )); do
    case "$1" in
    --use-wxphoenix=*)
        USE_WXPHOENIX="${1#*=}"
        shift
        ;;
    --install-xvfb=*)
        INSTALL_XVFB="${1#*=}"
        shift
        ;;
    --*) # unsupported flags
      echo "Error: Unknown flag $1" >&2
      exit 1
      break
      ;;
  esac
done


########################################################################
# Verify the options
########################################################################

# Print out the boolean logic
to_bool[0]="no"
to_bool[1]="yes"

echo "Configured options:"
echo "    Install wxPhoenix: ${to_bool[$USE_WXPHOENIX]}"
echo "    Install xvfb (X Virtual Framebuffer): ${to_bool[$INSTALL_XVFB]}"
echo ""


########################################################################
# Test if 'sudo' is available, work around otherwise
#
# Ubuntu docker images run as root and do not have the sudo command available
########################################################################
if ! command -v sudo >/dev/null 2>&1;
then
    # sudo is not available
    if [[ "$EUID" == "0" ]];
    then
        # We are running as root, so use a dummy function that makes 'sudo command' just call 'command'
        function sudo()
        {
            $@
        }
    else
        echo "Sudo is not available, please run this script as root." >&2
        exit 1
    fi
fi

########################################################################
# Install the packages
########################################################################

# Install development tools
PACKAGES="@development-tools \
    findutils \
    coreutils \
    grep \
    git \
    cmake \
    lemon \
    make \
    ninja-build \
    ccache \
    automake \
    gcc-c++ \
    gdb \
    clang \
    clang-libs \
    clang-tools-extra \
    git-clang-format \
    libasan \
    doxygen \
    graphviz"

# These are the core dependencies
PACKAGES="$PACKAGES \
    mesa-libGLw-devel \
    glew-devel \
    glm-devel \
    libcurl-devel \
    cairo-devel \
    boost-devel \
    openssl-devel \
    unixODBC-devel \
    gtk3-devel \
    opencascade-devel \
    swig \
    python3-devel \
    libgit2-devel \
    libsecret-devel \
    protobuf-devel \
    nng-devel \
    ngspice \
    libngspice \
    libngspice-devel \
    zint-devel"

# wxPhoenix
if [ $USE_WXPHOENIX == 1 ];
then
    PACKAGES="$PACKAGES python3-wxpython4"
fi

if [[ $VERSION_ID == "36" ]];
then
PACKAGES="$PACKAGES \
    wxGTK3-devel"
else
PACKAGES="$PACKAGES \
    wxGTK-devel"
fi

# Install the x virtual framebuffer if requested
if [ $INSTALL_XVFB == 1 ];
then
    PACKAGES="$PACKAGES \
        xorg-x11-server-Xvfb"
fi

echo "Installing packages: " $PACKAGES
echo ""

sudo dnf -y install $PACKAGES
